#!/usr/bin/perl -w
use strict;
use warnings "all";
use Getopt::Long;
use Text::CSV;
use Data::Dumper;

# By David Mercereau : https://david.mercereau.info/

# Version du programme (et des base)
my $version = '0.2'; my $dateVersion = 'Décembre 2022';

# parametres generaux du programme
my $params = {
    'version'        => $version,
    'dateversion'    => $dateVersion,
    'programme'      => 'cc2grisbi',
    'help'           => 0,
    'ignorestartrow'        => 1,
    'csvencoding'			=> 'ISO-8859-1',
    'source'		=> 'telechargement.csv',
    'destination'	=> 'telechargement-pour-grisbi.csv',
    'tiersregex'	=> 'tiers-clean-regex.txt',
    'libelle2categorie' => 'libelle2categorie.txt',
};

# Lecture des options ligne de commande
GetOptions(
    'help!'       => \$params->{help},
    'csvencoding:s'       => \$params->{csvencoding},
    'source:s'       => \$params->{source},
    'destination:s'       => \$params->{destination},
    'ignorestartrow:s'    => \$params->{ignorestartrow},
    'tiersregex:s'    => \$params->{tiersregex},
    'libelle2categorie:s'    => \$params->{libelle2categorie},
);

if ($params->{help} > 0) {
    print <<TEXTHELP;
Programme : $params->{programme}.pl V$params->{version} - ($params->{dateversion})
Perl version : $]

Usage : $params->{programme}.pl [Option ...]

  Option :
    -csvencoding=                : Encodage du CSV (par défaut ISO-8859-1)
    -source=                     : Fichier CSV source (par défaut ./telechargement.csv)
    -destination=                : Fichier CSV destination (par défaut ./telechargement-pour-grisbi.csv)
    -ignorestartrow=             : Nombre de ligne à ignorer au début (par défaut 5)
    -tiersregex=                 : Fichier pour le ménage sur les Tiers (par défaut ./tiers-clean-regex.txt)
    -libelle2categorie=          : Fichier pour l'attribution des catégorie selon le libellé (par défaut ./libelle2categorie.txt)
    -help                       : Affiche cette aide

TEXTHELP
    exit();
}

#####################################
### Programme
#####################################

my @rows;

# Lecture CSV
my $csv = Text::CSV->new ({ binary => 1, auto_diag => 1, sep_char => ";" });
open my $fh, "<:encoding($params->{csvencoding})", $params->{source} or die "$params->{source}: $!";
my $number_csv_line = 0;
while (my $row = $csv->getline ($fh)) {
	$number_csv_line = $number_csv_line + 1;
	#~ print $number_csv_line;
	if ($number_csv_line >= $params->{ignorestartrow}) { 
		# Gestion de l'entête
		if ($number_csv_line == $params->{ignorestartrow}) { 
			$row->[1] = 'N° Opération';
			$row->[2] = 'Remarques';
			$row->[3] = 'Débit';
			$row->[4] = 'Crédit';
			$row->[5] = 'Détail';
			$row->[6] = 'Tiers';
			$row->[7] = 'Catégorie';
			$row->[8] = 'Sous-catégorie';
			$row->[9] = '';
			$row->[10] = '';
			$row->[11] = '';
			$row->[12] = '';
		} else {
			$row->[6] = $row->[1];
			$row->[1] = $row->[3];
			if ($row->[4] eq '') {
			    $row->[2] = $row->[2];
			} else {
			    $row->[2] = $row->[2].' - '.$row->[4];
			    
			}
			$row->[3] = $row->[8];
			$row->[3] =~ s/^-//g;
			$row->[5] = '';
			$row->[4] = $row->[9];
			#~ $row->[4] =~ s/^\+//g;
			# Fait le ménage dans la colonne "Tiers"
			open(FH, '<', $params->{tiersregex}) or die $!;
			while(<FH>){
				# Si ce n'est pas un commentaire
				if (not $_ =~ m/^#/) {
					# On vire le saut de ligne
					$_ =~ s/\n//g;
					# On applique la regex
					$row->[6] =~ s/$_//g;
				}
			}
			close(FH);
			# Attribution d'une cathégorie
			$row->[7] = '';
			$row->[8] = '';
			open(FH, '<', $params->{libelle2categorie}) or die $!;
			while(<FH>){
				# Si ce n'est pas un commentaire
				if (not $_ =~ m/^#/) {
					# On vire le saut de ligne
					$_ =~ s/\n//g;
					# Si la regex correspond, on applique la catégorie ainsi que la sous catégorie
					my ($regex, $cat, $scat) = split /;/, $_;
					if ($row->[2] =~ m/$regex/) {
						$row->[7] = $cat;
						$row->[8] = $scat;
					}
				}
			}
			close(FH);
			$row->[9] = '';
			$row->[10] = '';
			$row->[11] = '';
			$row->[12] = '';
		}
		#~ if (not $row->[0] =~ m/^Solde/) {
		# Enregistrement des lignes
		push @rows, $row;
		#~ }
	}
	
    
}
close $fh;
 
# Ecriture du nouveau CSV
open $fh, ">:encoding($params->{csvencoding})", $params->{destination} or die "$params->{destination}: $!";
$csv->say ($fh, $_) for @rows;
close $fh or die "new.csv: $!";
