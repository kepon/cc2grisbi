# cc2grisbi - Crédit coopératif > Import Grisbi

cc2grisbi est un petit script perl qui permet transformer le fichier de sortie CSV du crédit coopératif en format mieux assimilable et déjà pré-rempli pour Grisbi

Que doit faire le script : 

- [x] Supprime les entête et pied de page du csv
- [x] Ignorer le "Solde" en fin de page
- [x] Fait le ménage dans les libellés et en sortir la colonne "Tiers". exemple :
  - Libellé : CB OVH             FACT XXXXX 
  - Déduit le Tiers : OVH
- [x] Renomme la colonne 'Libellé' en colonne "Remarque"
- [ ] Si le contenu de la colonne "Détail" n'est pas identique à Libelle alors on la concatène à la colonne "Remarque"
- [x] Supprimer les "+" et "-" des colonnes débit et crédit
- [x] Déduire à partir de la colonne  "Libellé" une colonne "Catégorie" et "Sous catégorie". Exemple : 
  - Libellé : CB OVH             FACT XXXXX 
  - Déduit la catégorie "Logement" et la sous catégorie "Internet"

## Utilisation

En entrée le CSV téléchargé sur l'espace Crédit Coopératif ressemble à ceci : 

```
Code de la banque : XXXX;Code de l'agence : XXX;Date de début de téléchargement : 01/04/2021;Date de fin de téléchargement : 30/04/2021;
Numéro de compte : XXXXXXXXXXX;Nom du compte : COMPTE DE DEPOT;Devise : EUR;

Solde en fin de période;;;;XXXX,88
Date;Numéro d'opération;Libellé;Débit;Crédit;Détail;
30/04/21;3004202120210430-14.32.15.710279 -;CB VISSERIE SERVIC FACT XXXX;-XX,16;;CB VISSERIE SERVIC FACT XXXX ;
30/04/21;3004202120210430-03.02.06.341595 -;VIR SEPA L OUVRE-BOITES 44;;+XXX,50;;
26/04/21;2604202120210426-17.06.18.846423 -;CB OVH             FACT XXXX;-X,20;;CB OVH             FACT XXXX ;
06/04/21;0604202120210406-17.02.23.859671 -;CB OVH             FACT XXXX;-XX,51;;CB OVH             FACT XXXX ;
06/04/21;0604202120210406-09.28.21.727796 -;VIR SEPA L OUVRE BOITE 44;;+XXX,63;;
Solde en début de période;;;;XXX,17
```

En sortie pour plaire à Grisbi il ressemble à cela : 

```
Date;"Numéro d'opération";Remarque;Débit;Crédit;Détail;Tiers;Catégorie;Sous-catégorie
30/04/21;"3004202120210430-14.32.15.710279 -";"CB VISSERIE SERVIC FACT XXX";83,16;;"CB VISSERIE SERVIC FACT XXXXX ";"VISSERIE SERVIC";Logement;Travaux
30/04/21;"3004202120210430-03.02.06.341595 -";"VIR SEPA L OUVRE-BOITES 44";;XXX,50;;"L OUVRE-BOITES 44";Salaire;
26/04/21;"2604202120210426-17.06.18.846423 -";"CB OVH             FACT 240421";X,20;;"CB OVH             FACT 240421 ";OVH;Logement;Internet
06/04/21;"0604202120210406-17.02.23.859671 -";"CB OVH             FACT 030421";XX,51;;"CB OVH             FACT 030421 ";OVH;Logement;Internet
06/04/21;"0604202120210406-09.27.33.639405 -";"VIR SEPA L OUVRE BOITE 44";;CC,28;;"L OUVRE BOITE 44";Salaire;
```

## Fonctionnement

Installer la librairie libtext-csv-perl : 

```bash
aptitude install libtext-csv-perl
```

Lancement du script : 

```bash
perl cc2grisbi.pl  -s fichier_source_telecharge_du_credit_cooperatif.csv -d fichier_destination_pour_grisbi.csv
```

Plus d'information sur les options avec le paramètre "--help": 

```
$ perl cc2grisbi.pl  -s test_telechargement.csv  --help
Programme : cc2grisbi.pl V0.1 - (Mai 2021)
Perl version : 5.030000

Usage : cc2grisbi.pl [Option ...]

  Option :
    -csvencoding=                : Encodage du CSV (par défaut ISO-8859-1)
    -source=                     : Fichier CSV source (par défaut ./telechargement.csv)
    -destination=                : Fichier CSV destination (par défaut ./telechargement-pour-grisbi.csv)
    -ignorestartrow=             : Nombre de ligne à ignorer au début (par défaut 5)
    -tiersregex=                 : Fichier pour le ménage sur les Tiers (par défaut ./tiers-clean-regex.txt)
    -libelle2categorie=          : Fichier pour l'attribution des catégorie selon le libellé (par défaut ./libelle2categorie.txt)
    -help                       : Affiche cette aide
```

Modifier le fichier libelle2categorie.txt pour attribuer des catégorie fonction d'un regex de libellé : 

```
# REGEX ; Catégorie ; Sous catégorie
VISSERIE;Logement;Travaux
^LEROY;Logement;Travaux
OUVRE-BOITES;Salaire;
OVH;Logement;Internet
MAIF;Assurance;Habitation
VIVAL;Alimentation;Supermarché
STATION;Automobile;Carburant
```

Modifier le fichier tiers-clean-regex.txt pour effectuer un nettoyage sur les libellés et ainsi déduire un tiers : 

```
^CB PAYPAL.+
^CB 
^PRLV 
CHEQUE.+
FACT [0-9]+
^VIR SEPA 
# Les espaces en début
^ 
# Les espaces en fin
\s+$
```

Import dans grisbi : 

* Fichier / Importer un fichier

* Ajouter un fichier

* Cocher importer "ISO-8859-1"

  ![](Readme.assets/Caracteres.png)

* Suivant 

* Point virgule <;> et attribuer les colonnes qui vont bien (grisbi mémorise ensuite ces choix)

  ![Importation des opérations par Grisbi (3 de 4)_262](./Readme.assets/Importation.png)

* Suivant et sélectionner "ajouter au compte XXX" (xxx = votre compte)

C'est importé !